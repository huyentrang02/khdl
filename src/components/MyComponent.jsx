import { LoadingOutlined, UploadOutlined } from "@ant-design/icons";
import { Button, Col, Image, Row, Spin, Upload, message } from "antd";
import axios from "axios";
import React, { useState } from "react";
import "../style/style.scss";

import bird01 from "../assets/bird01.jpg";
import bird02 from "../assets/bird02.jpg";
import bird03 from "../assets/bird03.png";
import bird04 from "../assets/bird04.png";
import bird05 from "../assets/bird05.png";
import bird06 from "../assets/bird06.jpg";

const topics = [
  {
    title: "Cắt Mỹ",
    color: "#96C144",
    src: bird01,
  },
  {
    title: "Chim én",
    color: "#96C144",
    src: bird02,
  },
  {
    title: "Chim Mỏ Sò Châu Phi",
    color: "#96C144",
    src: bird03,
  },
  {
    title: "Chim Sáo Mỹ",
    color: "#96C144",
    src: bird04,
  },
  {
    title: "Chim sẻ Họng Đen",
    color: "#96C144",
    src: bird05,
  },
  {
    title: "Ngỗng trời",
    color: "#96C144",
    src: bird06,
  },
];

export default function MyComponent() {
  const [answer, setAnswer] = useState("trống");
  const [isLoading, setIsLoading] = useState(false);
  const [fileList, setFileList] = useState([]);

  const handleUpload = () => {
    const formData = new FormData();
    formData.append("file", fileList[0]);
    setIsLoading(true);
    axios
      .post("http://127.0.0.1:8000/images", formData)
      .then((res) => {
        setAnswer(res.data);
      })
      .then(() => {
        setFileList([]);
        message.success("upload successfully.");
      })
      .catch(() => {
        message.error("upload failed.");
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  const props = {
    beforeUpload: (file) => {
      setFileList([file]);
      return false;
    },
    fileList,
  };

  return (
    <div className="wrapper-page">
      <div className="loading-spinner" hidden={isLoading}></div>
      <div className="container container__search">
        <div className="search-form">
          <Upload {...props} maxCount={1} listType="picture">
            <Button icon={<UploadOutlined />}>Select File</Button>
          </Upload>
          <Button
            type="primary"
            onClick={handleUpload}
            disabled={fileList.length === 0}
            loading={isLoading}
            style={{
              marginTop: 16,
            }}
          >
            {isLoading ? "Uploading" : "Start Upload"}
          </Button>
        </div>
        <div className="answer-box">
          <Row gutter={20}>
            {topics.map((topic) => {
              return (
                <Col className="gutter-row" span={4}>
                  <Spin
                    spinning={isLoading}
                    indicator={
                      <LoadingOutlined
                        style={{
                          fontSize: 24,
                        }}
                        spin
                      />
                    }
                  >
                    <div
                      className={`answer-box__item ${
                        answer !== topic.title && "transparent"
                      }`}
                      style={{ backgroundColor: topic.color }}
                    >
                      <h3>{topic.title}</h3>
                      <Image
                        style={{
                          borderRadius: "5px",
                          objectFit: "cover",
                          objectPosition: "center",
                        }}
                        height={250}
                        src={topic.src}
                        preview={false}
                      />
                    </div>
                  </Spin>
                </Col>
              );
            })}
          </Row>
        </div>
      </div>
    </div>
  );
}
